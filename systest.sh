#!/usr/bin/env bash
#
# Tmux session for testing ARMS
#

SESSION_NAME="systest"
CFGDIR="$HOME/config"

if ! tmux has-session -t $SESSION_NAME 2> /dev/null; then
    read -e -p 'Enter schedule file name> ' schedfile
    [[ -n "$schedfile" ]] || exit 0
    [[ -r "$CFGDIR/$schedfile" ]] || {
        echo "Schedule file not found!"
        exit 1
    }
    read -e -p 'Enter start time> ' tstart
    if [[ -n "$tstart" ]]; then
        now=$(date +%s)
        t0=$(date -d "$tstart" +%s)
        ((t0 <= now)) && {
            echo "Start time is in the past!"
            exit 1
        }
        ts=$(date -d "$tstart" +'%Y-%m-%dT%H:%M:%SZ')
        args=(" --start " "\"$ts\"" " arms.cfg" " $schedfile")
    else
        echo "Schedule will start immediately"
        args=("arms.cfg " "$schedfile")
    fi
    tmux new-session -s $SESSION_NAME -n main -d
    tmux split-window -v -t ${SESSION_NAME}:0
    pane="${SESSION_NAME}:0.top"
    tmux send-keys -t "$pane" "cd ~/config" C-m
    tmux send-keys -t "$pane" "runsched "
    tmux send-keys -t "$pane" "${args[@]}"
    tmux send-keys -t "$pane" " 2>&1 | svlogd -tt ~/logs/runsched" C-m
    pane="${SESSION_NAME}:0.bottom"
    tmux send-keys -t "$pane" "cd ~/logs && tail -f runsched/current" C-m
    tmux select-window -t ${SESSION_NAME}:0
fi
tmux attach -t $SESSION_NAME
