#!/usr/bin/env bash
#
# Run an ARMS schedule.
#

CFGDIR="$HOME/config"

read -e -p 'Enter schedule file name> ' schedfile
[[ -n "$schedfile" ]] || exit 0
[[ -r "$CFGDIR/$schedfile" ]] || {
    echo "Schedule file not found!"
    exit 1
}

# Clear any retained halt messages
mosquitto_pub -q 1 -t seq/commands/halt -n -r
mosquitto_pub -t schedule/commands/halt -n -r

# Strip the .sched extension
name="${schedfile%.*}"
systemctl --user start runsched@${name}
read -e -p 'Monitor log? [Y/n]> ' resp
case "$resp" in
    N|n|no)
        :
        ;;
    *)
        journalctl -f --user --user-unit runsched@${name}
        ;;
esac
