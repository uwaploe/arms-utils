#!/usr/bin/env bash
#
# Monitor MQTT channels and create a system status summary
#

: ${STATUSFILE=$HOME/logs/seqstatus}
: ${TMPFILE=$HOME/logs/.seqtmp}

seq=
att=
eng=
while read -r; do
    ts="$(date +'%Y-%m-%d %H:%M:%S')"
    done=
    set -- $REPLY
    case "$1" in
        seq/events/open)
            seq="${2##*/}"
            ;;
        eng/data)
            shift
            case "$1" in
                eng)
                    eng="$@"
                    ;;
                att)
                    att="$@"
                    ;;
            esac
            ;;
        seq/events/done)
            done=1
            seq=
            att=
            eng=
            ;;
    esac
    if [[ -n "$done" ]]; then
        printf '%s\nSequence done\n' "$ts" > "$TMPFILE"
    else
        printf '%s\nseq=%s\n%s\n%s\n' "$ts" "$seq" "$eng" "$att" > $TMPFILE
    fi
    mv "$TMPFILE" "$STATUSFILE"
done < <(mosquitto_sub -v -t eng/data -t 'seq/events/#')
