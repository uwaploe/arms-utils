#!/usr/bin/env bash
#
# Extract the engineering data from all sequences and package as
# a compressed TAR archive.
#

cd $HOME/data
for d in seq_*; do
    [[ -d "$d" ]] && \
        (
            echo -n "Processing $d ... "
            ts="${d#*_}"
            cd $d
            extract_eng.py eng_${ts}.txt *.h5
            echo "done"
        )
done

tar -c -v -z -f eng.tgz \
    --files-from=<(find . -name 'eng_*.txt' -print)
