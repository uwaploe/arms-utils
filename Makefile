
INSTALL = install

BINDIR = $(HOME)/bin
SVCDIR = $(HOME)/.config/systemd/user
CFGDIR = $(HOME)/config

PROGS = modemshell.sh stopseq.sh getrevs.sh \
	apply-bundles.sh seqstatus.sh modemlp.ksc \
    defaultsched.sh schedrun.sh extract_eng.py \
	arms_sched.py armscfg.py engdata.sh fallback.sh
SVCS := modemshell.service runsched@.service seqstatus.service tfoseq.service
CFGS := tfoseq-1.conf tfoseq-2.conf tfoseq-1-long.conf tfoseq-2-long.conf

install: install-bin install-ksc install-sv install-cfg

install-bin: $(PROGS)
	$(INSTALL) -d $(BINDIR)
	$(INSTALL) -m 755 -t $(BINDIR) $^

install-cfg: $(CFGS)
	$(INSTALL) -d $(CFGDIR)
	$(INSTALL) -m 644 -t $(CFGDIR) $^

install-ksc: modem_setup.ksc
	$(INSTALL) -m 644 modem_setup.ksc $(BINDIR)

install-sv: $(SVCS)
	install -d $(SVCDIR)
	install -b -m 644 -t $(SVCDIR) $^
	for s in $^; do \
	    [[ $$s = *@* ]] || \
	      systemctl --user enable $$s; \
	done
