#!/usr/bin/env bash
#
# Start the default schedule unless there is one already running.
#
export PATH=$HOME/bin:$HOME/venv/bin:$HOME/.nix-profile/bin:$PATH


# Get the name of the running schedule
getname() {
    systemctl --user --state=active --no-legend list-units 'runsched@*' |\
        sed -e 's/runsched@\([^.]*\)\.service.*/\1/'
}

name="$(getname)"
if [[ -n "$name" ]]; then
    echo "Schedule $name already running"
else
    echo "Starting the default schedule"
    systemctl --user start runsched@default
fi
