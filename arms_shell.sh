#!/usr/bin/env bash
#
# Simple command shell to run over the Acoustic Modem
#
export PATH=$HOME/venv/bin:$HOME/.nix-profile/bin:$PATH
export PYTHONPATH=$HOME/.nix-profile/lib/python2.7/site-packages

usage() {
    cat <<EOF
Usage: ${0##*/} [-h] [-m MODEMDEV] [-t TIMEOUT]

Simple shell to manage ARMS data collection. Reads commands entered
via the acoustic modem or standard input. TIMEOUT specifies the time
that the default schedule will be started unless a schedule is
started by command.
EOF
}

timespec='now + 2 hours'
modemdev=
OPTIND=1
while getopts hm:t: opt; do
    case $opt in
        h)
            usage
            exit 0
            ;;
        m)
            modemdev=$OPTARG
            ;;
        t)
            timespec="$OPTARG"
            ;;
        *)
            usage >&2
            exit 1
            ;;
    esac
done
shift "$((OPTIND-1))"

LOGDIR=$HOME/logs
CFGDIR=$HOME/config
CFGFILE=$CFGDIR/arms.cfg

t=$(date -d "$timespec" +%s)

[[ -n "$modemdev" && -e "$modemdev" ]] && {
    exec <> "$modemdev"
    # Disable echo on the high-latency modem connection
    stty -echo
}

# Disable interrupt, quit, and suspend chars.
stty -isig
declare -a cmd
bgpid=
mkdir -p $LOGDIR/runsched

while :; do
    t0=$(date +%s)
    # If no schedule is currently running, set a timeout on
    # the read command.
    if [[ -z "$bgpid" ]]; then
        timeout="-t $((t - t0))"
    else
        timeout=
    fi

    while read $timeout -p 'arms> ' -a cmd; do
        nargs="${#cmd[@]}"
        case "${cmd[0]}" in
            ping)
                echo "pong"
                ;;
            e|eng)
                eng $CFGDIR/arms.cfg
                ;;
            r|run)
                if kill -0 "$bgpid" 1> /dev/null 2>&1; then
                    echo "Schedule still running"
                else
                    file="${cmd[nargs-1]}"
                    if [[ -e "$CFGDIR/$file" ]]; then
                        pushd $CFGDIR 1> /dev/null 2>&1
                        runsched arms.cfg "$file" 2>&1 | svlogd -tt $LOGDIR/runsched &
                        bgpid=$!
                        popd 1> /dev/null 2>&1
                        echo
                    else
                        echo "Schedule $file not found"
                    fi
                fi
                ;;
            s|stat)
                if kill -0 "$bgpid" 1> /dev/null 2>&1; then
                    echo "running"
                else
                    echo "idle"
                fi
                ;;
            p|park)
                echo "Moving rotator to park position"
                pos="$(gcl-print -q $CFGFILE | \
                        grep 'Rotator.settings.park' | \
                        cut -f2 -d=)"
                if [[ -n "$pos" ]]; then
                    # strip spaces
                    read -rd '' pos <<< "$pos"
                    mkdir -p $LOGDIR/rostest
                    rostest /dev/ttyS2 "moveto $pos" "quit" 2>&1 |\
                        svlogd -tt $LOGDIR/rostest
                fi
                echo
                ;;
            halt)
                if kill -0 "$bgpid" 1> /dev/null 2>&1; then
                    mosquitto_pub -t seq/commands/halt -n
                    mosquitto_pub -t schedule/commands/halt -n
                    echo -n 'Waiting ...'
                    wait "$bgpid"
                    echo
                fi
                ;;
            xx)
                kill -0 "$bgpid" 1> /dev/null 2>&1 || exit 0
                ;;
            help)
                echo "Commands: eng, run, park, halt"
                ;;
            *)
                echo
                ;;
        esac
    done

    if kill -0 "$bgpid" 1> /dev/null 2>&1; then
        echo "Schedule running"
    else
        if [[ -e "$CFGDIR/default.sched" ]]; then
            echo "Starting default schedule"
            pushd $CFGDIR 1> /dev/null 2>&1
            runsched arms.cfg default.sched 2>&1 | svlogd -tt $LOGDIR/runsched &
            bgpid=$!
            popd 1> /dev/null 2>&1
        fi
    fi
done
