#!/usr/bin/env bash
#
# Simple command shell to run over the Acoustic Modem
#
export PATH=$HOME/bin:$HOME/venv/bin:$HOME/.nix-profile/bin:$PATH
export PYTHONPATH=$HOME/.nix-profile/lib/python2.7/site-packages

# Get the name of the running schedule
getname() {
    systemctl --user --state=active --no-legend list-units 'runsched@*' |\
        sed -e 's/runsched@\([^.]*\)\.service.*/\1/'
}

# Remove the default schedule from the atd queue
rmdefault() {
    while read jobid rest; do
        if at -c $jobid | grep defaultsched > /dev/null; then
            atrm $jobid && echo "Job $jobid removed"
        fi
    done < <(atq)
}

LOGDIR=$HOME/logs
CFGDIR=$HOME/config
CFGFILE=$CFGDIR/arms.cfg

# Disable echo, interrupt, quit, and suspend chars.
stty -isig -echo
declare -a cmd



echo 'Waiting for a connection' 1>&2
read str

echo "Listening for commands" 1>&2
echo -n 'arms> '
while read -a cmd; do
    echo "Command: ${cmd[@]}" 1>&2
    case "${cmd[0]}" in
        ping)
            echo "pong"
            ;;
        e|eng)
            eng $CFGDIR/arms.cfg
            ;;
        p|park)
            name="$(getname)"
            if [[ -n "$name" ]]; then
                echo "Schedule is running"
            else
                echo -n "Moving rotator to park position ... "
                pos="$(gcl-print -q $CFGFILE | \
                        grep 'Rotator.settings.park' | \
                        cut -f2 -d=)"
                if [[ -n "$pos" ]]; then
                    # strip spaces
                    read -rd '' pos <<< "$pos"
                    if rostest /dev/ttyS2 "stepto $pos" "quit" 1>&2; then
                        echo "done"
                    else
                        echo "failed"
                    fi
                fi
            fi
            ;;
        halt)
            name="$(getname)"
            if [[ -n "$name" ]]; then
                stopseq.sh && \
                    systemctl --user stop runsched@${name} && \
                    echo "Current sequence/schedule stopped"
            else
                rmdefault
            fi
            ;;
        r|run)
            # Clear any retained halt messages
            mosquitto_pub -q 1 -t seq/commands/halt -n -r
            mosquitto_pub -t schedule/commands/halt -n -r
            name="$(getname)"
            if [[ -n "$name" ]]; then
                echo "Schedule $name already running"
            else
                # Strip the .sched extension (if present)
                sname="${cmd[1]%.*}"
                [[ -n "$sname" ]] && \
                    systemctl --user start "runsched@${sname}" && \
                    rmdefault
                systemctl --user is-active -q "runsched@${sname}" && \
                    echo "Schedule $sname started"
            fi
            ;;
        stat)
            name="$(getname)"
            if [[ -n "$name" ]]; then
                echo "Schedule: ACTIVE ($name)"
                [[ -e $HOME/logs/seqstatus ]] && cat $HOME/logs/seqstatus
            else
                echo "Schedule: INACTIVE"
                [[ -e $HOME/logs/seqstatus ]] && cat $HOME/logs/seqstatus
            fi
            ;;
        tcm)
            name="$(getname)"
            if [[ -n "$name" ]]; then
                echo "Schedule is running"
            else
                tcmtest /dev/ttyS1 "orientation yup_180" "read" "quit"
            fi
            ;;
        *)
            ;;
    esac
    echo -n 'arms> '
done
