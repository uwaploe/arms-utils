#!/usr/bin/env bash
#
# Monitor MQTT channels and optionally log to a file.
#

logfile="$1"
[[ -n "$logfile" ]] && {
    opt=
    # Append if file exists
    [[ -e "$logfile" ]] && opt="-a"
    exec > >(tee "$opt" "$logfile")
}

while read -r; do
    printf '[%(%Y-%m-%d %H:%M:%S)T] %s\n' -1 "$REPLY"
done < <(mosquitto_sub -v -t eng/data -t 'seq/events/#')
