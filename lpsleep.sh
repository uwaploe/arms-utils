#!/usr/bin/env bash
#
# Send a message to the `lpmode` service to put the system into
# a low-power state until a specified time. The single command
# line argument is any valid GNU date input formats. See the URL
# below for examples:
#
# https://www.gnu.org/software/tar/manual/html_node/Date-input-formats.html
#
# The safest format is ISO 8601: YYYY-mm-ddTHH:MM:SS. Local timezone
# is assumed, for UTC, add a 'Z' to the end.
#

timespec="$1"
[[ -n "$timespec" ]] || {
    echo "Missing time specification" 1>&2
    exit 1
}

secs=$(date -d "$timespec" +%s)
[[ $? = "0" && -n "$secs" ]] || exit 1
echo "Sleeping until $(date -d @$secs --rfc-3339=seconds) ... "
exec mosquitto_pub -t system/lpmode/mem -m "$secs"
