#!/usr/bin/env bash
#
# Stop the runseq process cleanly.
#

# Send the 'halt' message
mosquitto_pub -q 1 -t seq/commands/halt -m "1" -r
mosquitto_pub -t schedule/commands/halt -m "1" -r

# Set a 60 second alarm
sleep 60 && kill -ALRM $$ 2> /dev/null &
alarm_pid=$!
# Wait for 'done' message
mosquitto_sub -q 1 -C 1 -t seq/events/done
kill $alarm_pid 2> /dev/null
