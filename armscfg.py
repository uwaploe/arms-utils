#!/usr/bin/env python
#
import os
import click
from pyarms.config import load_config, parse_sequence, \
    sequence_duration


@click.command()
@click.option('-v', '--verbose', is_flag=True)
@click.argument('cfgfile', type=click.File('rb'))
def cli(verbose, cfgfile):
    """
    Verify an ARMS configuration file. Exit status is 0
    if ok, non-zero on error.
    """
    status = 0
    path = os.environ.get('CFGPATH',
                          os.path.expanduser('~/config')).split(':')
    try:
        cfg = load_config(cfgfile, path=path)
    except Exception as e:
        status = str(e) if verbose else 1
    else:
        try:
            steps, settings = parse_sequence(cfg)
        except Exception as e:
            status = str(e) if verbose else 2
        else:
            if not steps[0].sample.rcvr.enable:
                status = 'ADC not enabled' if verbose else 3
            duration = sequence_duration(steps, speed=1.5)
            h, secs = divmod(int(duration), 3600)
            m, s = divmod(secs, 60)
            if verbose:
                sample = steps[0].sample
                click.echo('Steps = {:d}'.format(len(steps)))
                click.echo('Angles = {!r}'.format([st.angle for st in steps]))
                click.echo('Transmitter: {!r}'.format(sample.xmtr))
                click.echo('Receiver: {!r}'.format(sample.rcvr))
                click.echo('Approx. duration = {:02d}h{:02d}m{:02d}s'.format(
                    h, m, s))

    raise SystemExit(status)


if __name__ == '__main__':
    cli()
