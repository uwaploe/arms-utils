#!/usr/bin/env bash
#
# Get the latest revision ID for all git repositories
#

: ${PROJDIR=$HOME/src}

dirs=($PROJDIR/*)
for d in "${dirs[@]}"; do
    if [[ -d "$d/.git" ]]; then
        echo -n "${d##*/} $(GIT_DIR="$d/.git" git log -1 --pretty=%h) "
        GIT_DIR="$d/.git" git remote get-url origin 2> /dev/null || \
            GIT_DIR="$d/.git" git remote show -n origin |\
                grep 'Fetch' | cut -f2- -d:
    fi
done
