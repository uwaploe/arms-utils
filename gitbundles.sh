#!/usr/bin/env bash
#
# Create Git bundles for ARMS
#

: ${PROJDIR=$HOME/projects/ARMS}

usage() {
    cat <<EOF
Usage: ${0##*/} [-h] BUNDLE_DIR INFILE

Read a series of lines from INFILE with the following fields:

REPO_NAME REVISION REMOTE_URL

For each listed repository, create a Git "bundle" which can be
uploaded to the ARMS system and used to bring the repository up
to date. If the repository does not exist on the local system it
will be cloned from the remote URL.
EOF
}

[[ "$1" = "-h" ]] && {
    usage
    exit 0
}

bundle_dir="$1"
infile="$2"
[[ -n "$bundle_dir" && -n "$infile" ]] || {
    usage
    exit 1
}

mkdir -p "$bundle_dir"
n=0
while read repo rev url; do
    ref=
    if [[ -n "$url" ]]; then
        mkdir -p /tmp/repos
        [[ -d $PROJDIR/$repo ]] && ref="--reference $PROJDIR/$repo"
        repodir="/tmp/repos/$repo"
        rm -rf "$repodir"
        git clone $ref $url $repodir
    else
        repodir="$PROJDIR/$repo"
    fi
    b=$(GIT_DIR="$repodir/.git" git rev-parse --abbrev-ref HEAD)
    if [[ "$rev" = "0" ]]; then
        GIT_DIR="$repodir/.git" git bundle create \
               "$bundle_dir/${repo}.bundle" ${b} && ((n++))
    else
        GIT_DIR="$repodir/.git" git bundle create \
               "$bundle_dir/${repo}.bundle" ${rev}..${b} && ((n++))
    fi
done < "$infile"

if ((n > 0)); then
    echo "Creating Git bundles"
    (
        arfile="$(pwd)/bundles.tar.gz"
        cd $bundle_dir
        tar -c -f $arfile -v -z *.bundle
    )
    rm -rf $bundle_dir
else
    echo "All Git repos are up-to-date"
fi