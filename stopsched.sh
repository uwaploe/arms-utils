#!/usr/bin/env bash
#
# Stop the current ARMS schedule. If the '--force' option is
# given, stop the schedule ASAP, otherwise, wait until the
# current entry has finished.
#

[[ "$1" = "--force" ]] && {
    mosquitto_pub -t seq/commands/halt -n
}
mosquitto_pub -t schedule/commands/halt -n
