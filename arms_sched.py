#!/usr/bin/env python
#
import os
import click
from pyarms.config import load_config, parse_sequence, \
    sequence_duration, parse_schedule


@click.command()
@click.option('-v', '--verbose', count=True)
@click.argument('fsched', type=click.File('rb'))
def cli(verbose, fsched):
    """
    Verify an ARMS configuration file. Exit status is 0
    if ok, non-zero on error.
    """
    status = 0
    path = os.environ.get('CFGPATH',
                          os.path.expanduser('~/config')).split(':')

    try:
        sched = list(parse_schedule(fsched))
    except Exception as e:
        click.secho(str(e), fg='red')
        raise SystemExit(-1)

    count = 0
    for row in sched:
        count += 1
        click.secho('Checking: {}'.format(row['name']), fg='yellow')
        with open(row['name'], 'rb') as cfgfile:
            try:
                cfg = load_config(cfgfile, path=path)
            except Exception as e:
                click.secho(str(e), fg='red')
                status = count
            else:
                try:
                    steps, settings = parse_sequence(cfg)
                except Exception as e:
                    click.secho(str(e), fg='red')
                    status = count
                else:
                    duration = sequence_duration(steps, speed=1.4)
                    h, secs = divmod(int(duration), 3600)
                    m, s = divmod(secs, 60)
                    if verbose:
                        sample = steps[0].sample
                        click.echo('Steps = {:d}'.format(len(steps)))
                        if verbose > 1:
                            click.echo('Angles = {!r}'.format(
                                [st.angle for st in steps]))
                        click.secho('Transmitter: {!r}'.format(sample.xmtr),
                                    fg='green')
                        click.secho('Receiver: {!r}'.format(sample.rcvr),
                                    fg='green')
                        click.echo('Duration (est): {:02d}h{:02d}m{:02d}s'.format(
                            h, m, s))

    raise SystemExit(status)


if __name__ == '__main__':
    cli()
