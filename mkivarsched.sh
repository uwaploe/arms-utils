#!/usr/bin/env bash
#
# Create a set of SCFs and schedule file for transmitting to IVAR
#

: ${CFGDIR=$HOME/config}

usage() {
    cat <<EOF
Usage: mkivarsched.sh TEMPLATE ANGLE PLIST

Create a set of SCFs and a schedule file for transmitting to IVAR,
TEMPLATE is a template SCF, ANGLE is the rotator angle in integer
degrees, and PLIST is a file containing a single column list of
pulse file names. The schedule file and SCFs are written to the
configuration directory.
EOF
    exit 1
}

[[ $# -eq "3" ]] || usage

template="$1"
angle="$2"
plist="$3"

[[ -f "$template" ]] || {
    echo "Template file \"$template\" not found" 1>&2
    exit 2
}

[[ -f "$plist" ]] || {
    echo "Waveform list file \"$plist\" not found" 1>&2
    exit 2
}

((angle < 17 || angle > 287)) && {
    echo "Invalid rotator angle: $angle" 1>&2
    exit 3
}

sched=$(printf "ivar_%03d.sched" "$angle")
echo "name,interval,reverse,count,timeout" > $CFGDIR/$sched

count=1
while read waveform; do
    if [[ -n "$waveform" ]]; then
        scf=$(printf "ivar_%03d_seq%02d.cfg" "$angle" "$count")
        echo "Creating $scf ..."
        [[ -e "$HOME/pulses/$waveform" ]] || \
            echo "WARNING: waveform file $waveform not found"
        sed -e "s!@ANGLE@!${angle}!g" \
            -e "s!@WAVEFORM@!${waveform}!g" \
            "$template" > $CFGDIR/$scf
        echo "$scf,00:00,0,1,00:10" >> $CFGDIR/$sched
        ((count++))
    fi
done < "$plist"
echo "New schedule: $CFGDIR/$sched"
