#!/usr/bin/env bash
#
# Provide a MQTT-based service to put the system into a low power sleep
# state.
#
# This service subscribes to the topics "system/lpmode/+" where the last
# component of the topic is the sleep mode (see rtcwake(1)) and the
# message payload is either the number of seconds to sleep (if preceeded
# by a '+') or the absolute wake-up time in seconds since the epoch.
#

TOPIC='system/lpmode/+'
MODS=("iguana_adc")
echo "Listening for messages on $TOPIC"

while read topic payload; do
    mode="${topic##*/}"
    if [[ "${payload:0:1}" = '+' ]]; then
        opts="-s ${payload:1}"
    else
        opts="-t $payload"
    fi
    modprobe -r "${MODS[@]}"
    echo "rtcwake -m $mode $opts"
    rtcwake -m $mode $opts
    for m in "${MODS[@]}"; do
        modprobe "$m"
    done
done < <(stdbuf -oL mosquitto_sub -v -t "$TOPIC")
