#!/usr/bin/env bash
#
# Arrange for the default schedule file to start sometime
# in the future.
#

CFGDIR="$HOME/config"

rmdefault() {
    while read jobid rest; do
        if at -c $jobid | grep defaultsched > /dev/null; then
            atrm $jobid
        fi
    done < <(atq)
}

[[ -r "$CFGDIR/default.sched" ]] || {
    echo "Default schedule file not found!"
    exit 1
}

read -e -p 'Enter default start time> ' tstart
if [[ -n "$tstart" ]]; then
    now=$(date +%s)
    t0=$(date -d "$tstart" +%s)
    [[ $? = "0" ]] || {
        echo "Invalid date/time specification"
        exit 1
    }
    ((t0 <= now)) && {
        echo "Start time is in the past!"
        exit 1
    }
    ts=$(date -d "$tstart" +'%H:%M %Y-%m-%d')
    # Remove any existing entries first
    rmdefault
    echo "$HOME/bin/defaultsched.sh > /dev/null" | at -M "$ts"
else
    echo "No start time entered ..."
fi
